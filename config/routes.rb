Rails.application.routes.draw do
  get 'site/index'
  get 'site/login'
  get 'site/talents'
  root 'site#index'
  resources :competitions do
  	resources :registrations do
      resources :homologations, only: [:create]
    end
    resources :participations do
      resources :checkin, only: [:create]
    end
    resources :homologations, only: [:index]
    resources :checkin, only: [:index]
    resources :certificates, only: [:index]
  end
  resources :institutions
  devise_for :admins
  devise_for :delegates
  devise_for :competitors
end
