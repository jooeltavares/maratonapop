require "application_system_test_case"

class CompetitionsTest < ApplicationSystemTestCase
  setup do
    @competition = competitions(:one)
  end

  test "visiting the index" do
    visit competitions_url
    assert_selector "h1", text: "Competitions"
  end

  test "creating a Competition" do
    visit competitions_url
    click_on "New Competition"

    fill_in "Date", with: @competition.date
    fill_in "Duration", with: @competition.duration
    fill_in "Edition", with: @competition.edition
    fill_in "Level", with: @competition.level
    fill_in "Registration End", with: @competition.registration_end
    fill_in "Registration Start", with: @competition.registration_start
    click_on "Create Competition"

    assert_text "Competition was successfully created"
    click_on "Back"
  end

  test "updating a Competition" do
    visit competitions_url
    click_on "Edit", match: :first

    fill_in "Date", with: @competition.date
    fill_in "Duration", with: @competition.duration
    fill_in "Edition", with: @competition.edition
    fill_in "Level", with: @competition.level
    fill_in "Registration End", with: @competition.registration_end
    fill_in "Registration Start", with: @competition.registration_start
    click_on "Update Competition"

    assert_text "Competition was successfully updated"
    click_on "Back"
  end

  test "destroying a Competition" do
    visit competitions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Competition was successfully destroyed"
  end
end
