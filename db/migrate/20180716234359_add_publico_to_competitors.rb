class AddPublicoToCompetitors < ActiveRecord::Migration[5.2]
  def change
    add_column :competitors, :publico, :boolean
  end
end
