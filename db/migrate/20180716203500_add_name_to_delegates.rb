class AddNameToDelegates < ActiveRecord::Migration[5.2]
  def change
    add_column :delegates, :name, :string
  end
end
