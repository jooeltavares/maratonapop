class AddInstitutionRefToCompetitors < ActiveRecord::Migration[5.2]
  def change
    add_reference :competitors, :institution, foreign_key: true
  end
end
