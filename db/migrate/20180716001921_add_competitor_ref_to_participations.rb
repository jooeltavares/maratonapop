class AddCompetitorRefToParticipations < ActiveRecord::Migration[5.2]
  def change
    add_reference :participations, :competitor, foreign_key: true
  end
end
