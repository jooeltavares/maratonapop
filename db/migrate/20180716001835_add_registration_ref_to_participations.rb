class AddRegistrationRefToParticipations < ActiveRecord::Migration[5.2]
  def change
    add_reference :participations, :registration, foreign_key: true
  end
end
