class CreateInstitutions < ActiveRecord::Migration[5.2]
  def change
    create_table :institutions do |t|
      t.string :name
      t.string :website
      t.string :city
      t.string :state

      t.timestamps
    end
  end
end
