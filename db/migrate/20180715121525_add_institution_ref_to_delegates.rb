class AddInstitutionRefToDelegates < ActiveRecord::Migration[5.2]
  def change
    add_reference :delegates, :institution, foreign_key: true
  end
end
