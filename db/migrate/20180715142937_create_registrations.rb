class CreateRegistrations < ActiveRecord::Migration[5.2]
  def change
    create_table :registrations do |t|
      t.string :time
      t.timestamp :data
      t.string :homologada
      t.references :competition, foreign_key: true
      t.references :delegate, foreign_key: true

      t.timestamps
    end
  end
end
