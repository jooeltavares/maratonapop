class CreateCompetitions < ActiveRecord::Migration[5.2]
  def change
    create_table :competitions do |t|
      t.string :edition
      t.string :level
      t.datetime :date
      t.integer :duration
      t.datetime :registration_start
      t.datetime :registration_end

      t.timestamps
    end
  end
end
