class AddNameToCompetitors < ActiveRecord::Migration[5.2]
  def change
    add_column :competitors, :name, :string
  end
end
