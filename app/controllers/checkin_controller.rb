class CheckinController < ApplicationController
  before_action :authenticate_admin!
  def index
  	@competition = Competition.find(params[:competition_id])
  end

  def create
  	competition = Competition.find(params[:competition_id])
  	participation = Participation.find(params[:participation_id])
  	if participation.presenca.nil?
  		participation.presenca = params[:status]
  		participation.save
  	end
  	redirect_to competition_checkin_index_path(competition)
  end
end
