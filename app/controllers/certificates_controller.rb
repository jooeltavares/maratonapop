class CertificatesController < ApplicationController
  before_action :authenticate_competitor!
  def index

  	
  	@current_competitor = current_competitor
  	@competition = Competition.find(params[:competition_id])
  	render :layout => false
  end
end
