class RegistrationsController < ApplicationController
  before_action :authenticate_delegate!
  before_action :set_registration, only: [:show, :edit, :update, :destroy]
  before_action :set_competitors

  # GET /registrations
  # GET /registrations.json
  def index
    @competition = Competition.find(params[:competition_id])
    @registrations = @competition.registrations
  end

  # GET /registrations/1
  # GET /registrations/1.json
  def show
  end

  # GET /registrations/new
  def new
    @registration = Registration.new
    @registration.competition = Competition.find(params[:competition_id])
  end

  # GET /registrations/1/edit
  def edit
  end

  # POST /registrations
  # POST /registrations.json
  def create
    @registration = Registration.new(registration_params)
    @registration.competition = Competition.find(params[:competition_id])
    @registration.data = Time.now.getutc
    @registration.delegate = current_delegate
    respond_to do |format|
      begin
        validate_time_name_create(@registration)
        validates_participations(@registration)
        if @registration.save
          create_participations(@registration)
          format.html { redirect_to competition_registration_path(@registration.competition, @registration), notice: 'Registration was successfully created.' }
          format.json { render :show, status: :created, location: @registration }
        else
          format.html { render :new }
          format.json { render json: @registration.errors, status: :unprocessable_entity }
        end
        rescue => e
          flash[:notice] = e.message
          format.html { render :new }
      end
    end
  end

  # PATCH/PUT /registrations/1
  # PATCH/PUT /registrations/1.json
  def update
    respond_to do |format|
      begin
        validate_time_name_update(@registration)
        validates_participations(@registration)
        if @registration.update(registration_params)
          destroy_create_participations(@registration)
          create_participations(@registration)
          format.html { redirect_to competition_registration_path(@registration.competition, @registration), notice: 'Registration was successfully updated.' }
          format.json { render :show, status: :ok, location: @registration }
        else
          format.html { render :edit }
          format.json { render json: @registration.errors, status: :unprocessable_entity }
        end
      rescue => e
        flash[:notice] = e.message
        format.html { render :edit }
        # format.json { render json: @registration.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /registrations/1
  # DELETE /registrations/1.json
  def destroy
    @registration.participations.destroy_all
    @registration.destroy
    respond_to do |format|
      format.html { redirect_to registrations_url, notice: 'Registration was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_registration
      @registration = Registration.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def registration_params
      params.require(:registration).permit(:time, :data, :homologada, :competition_id, :delegate_id)
    end

    def create_participations(registration)
        competitors = Competitor.where(id: params[:competitors_ids])
        competitors.each do |competitor|
          participation = Participation.new
          participation.registration = registration
          participation.competitor = competitor
          participation.save
        end
    end

    def destroy_create_participations(registration)
      registration.participations.destroy_all
    end

    def validates_participations(registration)
      total = if params[:competitors_ids] then params[:competitors_ids].count else 0 end
      if registration.competition.level == 'M' and total > 2
        raise 'No maximo até 2 competidores'
      elsif registration.competition.level == 'S' and total > 3
        raise 'No maximo até 3 competidores'
      end
    end

    def validate_time_name_update(registration)
        r = registration.competition.registrations.find_by(time: params[:registration][:time])
        if r and r.id != registration.id
          raise 'Time já cadastrado'
        end
    end

    def validate_time_name_create(registration)
      if registration.competition.registrations.find_by(time: params[:registration][:time])
          raise 'Time já cadastrado'
        end
    end

    def set_competitors
      @competitors = Competitor.joins(participations: :registration).where('registrations.competition_id' => params[:competition_id]).ids
    end
end
