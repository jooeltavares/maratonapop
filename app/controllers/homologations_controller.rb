class HomologationsController < ApplicationController
  before_action :authenticate_admin!
  def index
  	@competition = Competition.find(params[:competition_id])
  end

  def create
  	registration = Registration.find(params[:registration_id])
  	if registration.homologada.nil?
  		registration.homologada = params[:status]
  		registration.save
  	end
  	redirect_to competition_homologations_path(registration.competition)
  end
end
