class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :configure_permitted_update_params, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :publico, :matricula, :institution_id, contacts_attributes: [:id, :tipo, :value, :_destroy]])
  end

   def configure_permitted_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :publico, :matricula, :institution_id, contacts_attributes: [:id, :tipo, :value, :_destroy]])
   end
end
