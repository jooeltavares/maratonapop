class Competition < ApplicationRecord
  validates :edition, presence: true
  validates :duration, presence: true
  has_many :registrations
end
