class Competitor < ApplicationRecord
  belongs_to :institution
  has_many :participations
  has_many :contacts
  # validates :institution_id, presence: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  accepts_nested_attributes_for :contacts, reject_if: :all_blank, allow_destroy: true
end
