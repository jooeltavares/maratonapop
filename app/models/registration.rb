class Registration < ApplicationRecord
  validates :time, presence: true
  belongs_to :competition
  belongs_to :delegate
  has_many :participations
end
