class Institution < ApplicationRecord
	validates :name, presence: true
	validates :city, presence: true
	validates :state, presence: true
    has_many :competitors
    has_many :delegates
end
