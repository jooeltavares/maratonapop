class Contact < ApplicationRecord
  validates :tipo, presence: true
  validates :value, presence: true
  belongs_to :competitor
end
