json.extract! institution, :id, :name, :website, :city, :state, :created_at, :updated_at
json.url institution_url(institution, format: :json)
