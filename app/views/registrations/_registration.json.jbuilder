json.extract! registration, :id, :time, :data, :homologada, :competition_id, :delegate_id, :created_at, :updated_at
json.url registration_url(registration, format: :json)
