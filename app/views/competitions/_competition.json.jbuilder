json.extract! competition, :id, :edition, :level, :date, :duration, :registration_start, :registration_end, :created_at, :updated_at
json.url competition_url(competition, format: :json)
